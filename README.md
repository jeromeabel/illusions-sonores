# Illusions-sonores

Tout comme les illusions visuelles, les illusions sonores jouent sur nos perceptions pour faire émerger d'étranges sensations et brouiller nos repères : filtres, masques, décalages, spatialisation, interférences, etc. En s'inspirant et en expérimentant ces phénomènes, ce workshop sera l'occasion de concevoir et prototyper des installations sonores et interactives. Nous utiliserons le langage de programmation multimédia Pure Data, qui nous donne accès aux fonctions élémentaires du son. Pour protoyper les idées, nous verrons comment mettre en scène le rendu sonore et comment créer des interfaces MIDI pour récupérer les informations de différents capteurs. 

Objectifs pédagogiques :
- Comprendre les bases physiques du son
- Découvrir le langage de programmation Pure Data, "couteau suisse" multimédia pour les artistes
- Concevoir et réaliser des installations sonores et interactives

## Installation
- Logiciel Pure Data 0.52.1 : http://msp.ucsd.edu/software.html
- Menu Aide > Installer des objets supplémentaires : else, earplug~

## Sources des images
Beaucoup d'images proviennent de livres que je nomme en crochets ici pour économiser de la place. Il sont cités par la suite dans la bibliographie.

- acoustic-architecture.gif : [Designing Sound]
- anechoic.gif : http://upload.wikimedia.org/wikipedia/commons/a/aa/IBM_Anechoic_chamber.jpg
- delay.gif : [Designing Sound]
- onde-2.gif : http://acoustique.archi.free.fr/images/008.jpg
- onde-3.gif : http://a10.idata.over-blog.com/0/12/56/84/super/Image222.gif
- onde.gif : illustration personnelle sur la base de http://a401.idata.over-blog.com/600x286/5/98/05/86/588e342c-d462-403f-ab05-ec91cbfc3cd1-PhenomenePhysique_Sche.jpg
- oreille.gif : [Audionumerique]
- propagation-of-waves.gif : [Designing Sound]
- reverb.gif : [Audionumerique]
- sincos.gif : http://abcmaths.free.fr/1eres/sincos.gif
- sonagramme.gif : [Acoustique]
- spectre.gif : http://scheeline.scs.illinois.edu/~asweb/CPS/HSFiles/electromagnetic-spectrum.jpg
- temperature.gif : [Life]
- time.gif : ?
- visualisation.gif : http://upload.wikimedia.org/wikipedia/commons/8/85/Sinus-visualisation.gif
- vitesse-du-son.gif : [Acoustique]

- acoustic-psychoacoustic.gif : http://www.csis.ul.ie/coursemodule/CS5611
- acoustis-ecology.gif : http://www.csis.ul.ie/coursemodule/CS5611
- additive-synthesis.gif : [Designing Sound]
- battements.gif : [Life]
- cochlee-frequences.gif : http://wikis.lib.ncsu.edu/images/d/d6/I10-85-cochlea2.jpg
- cochlee.gif : http://web2.crdp.ac-versailles.fr/arts/domaines/sciences-techniques/VOLUTE%20-%20LA%20COCHLEE.JPG
- decibel.gif : http://www.sonorisation-spectacle.org/relation-de-puissance.html
- decomposition.gif : ?
- decroissance.gif : [Acoustique]
- deutsch.gif : http://auriol.free.fr/psychosonique/colloque-subjectif/baruch.htm
- doppler.gif : [Life]
- doppler-shift.gif : [Designing Sound] 
- equations.gif : http://deptinfo.cnam.fr/new/spip.php?pdoc4035
- formants.gif : [Effets sonores]
- formants-voyelles.gif : [Acoustique]
- frequences.gif : [Audionumerique]
- interferences.gif : [Life]
- intervalles.gif : [Life]
- linear-log.gif : http://www.csis.ul.ie/coursemodule/CS5611
- listening-modes.gif : http://www.csis.ul.ie/coursemodule/CS5611
- localisation.gif : [Audionumerique]
- localisations-subjectives.gif : http://auriol.free.fr/psychosonique/ClefDesSons/localisations-subjectives.jpg
- loudness.gif : http://www.csis.ul.ie/coursemodule/CS5611
- masquage-temporel.gif : 
- masque-frequentiel-f0.gif : [Effets sonores]
- musical-instrument-model.gif : http://www.csis.ul.ie/coursemodule/CS5611
- nautile.gif : Livre "Le code secret - La formule mystérieuse qui régit les arts, la nature et les sciences", Priya Hemenway
- orchestres.gif : [Acoustique]
- oreille.gif : [Audionumerique]
- panning-laws.gif : [Designing Sound] 
- pd-spatialisation.gif : patch ambisonic
- penrose-staircase.gif : http://en.wikipedia.org/wiki/File:Impossible_staircase.svg
- perception-log.gif : [Acoustique]
- phase-annulation.gif : http://www.csounds.com/tootsother/east/Graphics/figures/204.gif
- resonances.gif : [Effets sonores]
- resonnance-applications.gif : [Effets sonores]
- reverb.gif : [Audionumerique]
- tessiture.gif : http://upload.wikimedia.org/wikipedia/fr/timeline/106fb71772cbebb44825912708c1fc96.png 
- tessiture-voix.gif : http://www.vocalises.net/IMG/jpg/schema-des-principales-voix-2.jpg
- waveforms-harmonics.gif : [Designing Sound] 

- adc-dac.gif : [Audionumerique]
- adsr.gif : http://upload.wikimedia.org/wikipedia/fr/e/e4/Enveloppe_ADSR.png
- delay.gif : [Audionumerique]
- echantillonnage.gif : [Audionumerique]
- echantillonnage-probleme.gif : [Audionumerique]
- passe-haut.gif : [Audionumerique]
- quantification-fidelite.gif : [Audionumerique]

## Livres
- [Effets sonores]	AUGOYARD Jean-François, A l'écoute de l'environnement, répertoire des effets sonores, Parenthèses, 1989
- [Audionumerique]	DE REYDELLET Jean, ROADS Curtis, L'audionumérique, Musique et informatique, Dunod, 2007
- [Designing Sound]	FARNELL Andy, Designing Sound, Applied Scientific Press, 2010
- [Acoustique]		FISCHETTI Antonio, Initiation à l'acoustique, Belin, 2004
- [Life] 		STEVENS Stevens, WARSHOFSKY Fred, Le Son et l'audition, Life, le monde des sciences, 1966

## Liens
- CS5611 - Acoustics and Psychoacoustics : http://www.csis.ul.ie/coursemodule/CS5611
- Handbook for acoustic ecology : http://www.sfu.ca/sonic-studio/handbook/ 
- Math, Physics, and Engineering Applets : http://www.falstad.com/mathphysics.html 
- Musical Illusions and Paradoxes Diana Deutsch : http://philomel.com/musical_illusions/
- Sons illusoires et illusions sonores : http://auriol.free.fr/psychosonique/colloque-subjectif/baruch.htm
- Atelier Cyrille Henry, Nicolas Montgermont : http://drpichon.free.fr/ch/article.php?id_article=50
- Sources sonores : http://www.techniquesduson.com/sourcessonores.html
- Designing Sound : http://mitpress.mit.edu/catalog/item/default.asp?ttype=2&tid=12282
- La nature du son musical : http://www.musinfo.fr/archives/anciensite/texte/theses.htm 
- Qu'est-ce que le son : http://www.je-comprends-enfin.fr/index.php?/Eau-ondes-et-son/quest-ce-que-le-son/id-menu-15.html
- Définition du son : http://www.sonorisation-spectacle.org/definition-du-son.html
- Applet : http://web.cortial.net/bibliohtml/sonlon_j.html
- Top 10 Incredible Sound Illusions : http://listverse.com/2008/02/29/top-10-incredible-sound-illusions/
- Pure Data, cours sur la spatialisation : http://codelab.fr/2627

## Vidéos
- Endless Glissando (Shepard tone, Jean Claude Risset) : http://www.youtube.com/watch?v=iupWWsh8YCo
- Understanding sound waves : http://www.youtube.com/watch?v=4iIE1Rm__-E
‪- Sound, Vibration, Wave Characteristics‬: http://www.youtube.com/watch?v=dbeK1fg1Rew
- First computer to sing - Daisy Bell : http://www.youtube.com/watch?v=41U78QP8nBk

